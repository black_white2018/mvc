<?php
	return [
		'' => [
			'controller' => 'index',
			'action' => 'main',
			'ru' => 'Главная'
		],
		'news' => [
			'controller' => 'news',
			'action' => 'main',
			'ru' => 'Новости'
		],
		'price' => [
			'controller' => 'price',
			'action' => 'main',
			'ru' => 'Прайс - лист'
		],
		'delivery' => [
			'controller' => 'delivery',
			'action' => 'main',
			'ru' => 'Доставка'
		],
		'contacts' => [
			'controller' => 'contacts',
			'action' => 'main',
			'ru' => 'Контакты'
		]
	];