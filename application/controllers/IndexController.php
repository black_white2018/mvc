<?php
	namespace application\controllers;

	use application\core\Controller;

	class IndexController extends Controller
	{
		public function mainAction() {

			$vars = [
				'title' => 'Главная',
				'description' => 'описание главной стр',
				'keywords' => 'ключевые слова главной стр'
			];
			$this->view->render($vars);
			$this->model->run();
		}
	}