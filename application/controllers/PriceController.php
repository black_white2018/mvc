<?php
	namespace application\controllers;

	use application\core\Controller;

	class PriceController extends Controller
	{
		public function mainAction() {
			$vars = [
				'title' => 'Прайс - лист',
				'description' => 'описание',
				'keywords' => 'ключевые слова '
			];
			$this->view->render($vars);
		}
	}