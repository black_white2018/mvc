<?php
	namespace application\controllers;

	use application\core\Controller;

	class DeliveryController extends Controller
	{
		public function mainAction() {
			$vars = [
				'title' => 'Доставка',
				'description' => 'описание',
				'keywords' => 'ключевые слова '
			];
			$this->view->render($vars);
		}
	}