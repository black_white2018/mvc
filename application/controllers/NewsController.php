<?php
	namespace application\controllers;

	use application\core\Controller;

	class NewsController extends Controller
	{
		public function mainAction() {
			$vars = [
				'title' => 'Новости',
				'description' => 'описание новости',
				'keywords' => 'ключевые слова '
			];
			$this->view->render($vars);
		}
	}