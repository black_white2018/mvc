<?php
	namespace application\controllers;

	use application\core\Controller;

	class ContactsController extends Controller
	{
		public function mainAction() {
			$vars = [
				'title' => 'Контакты',
				'description' => 'описание',
				'keywords' => 'ключевые слова '
			];
			$this->view->render($vars);
		}
	}