<?php
	namespace application\core;

	use application\core\ViewInterface;

	class View implements ViewInterface
	{
		private $layout = 'default';
		private $path;
		private $params;

		public function __construct(array $params) {
			$this->params = $params;
		}

		public function render(array $vars = []) {
			extract($vars);
			$url = $this->params;
			$path = 'application/views/pages/'.$this->params['controller'].'/'.$this->params['action'].'.php';
			if (file_exists($path)) {
				ob_start();
				require $path;
				$content = ob_get_clean();
			}
			require 'application/views/layouts/default.php';
		}

		public static function errorCode(int $code = 404) {
			http_response_code($code);
			require 'application/views/errors/'.$code.'.php';
		}
	}