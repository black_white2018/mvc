<?php
	/**
		* не готово к использованию
	*/
	namespace application\core;

	class Db
	{
		private $pdo;

		public function construct() {
			try {
				$option = require 'application/config/db.php';
				$this->pdo = new PDO('mysql:host='.$option['host'].';dbname='.$option['dbname'].'', $option['dbuser'], $option['dbpassword']);				
			} catch (PDOException $error) {
				die($error->getMessage());
			}			
		}
		
		/*
			* требуется протестить
		*/

		public function query(string $sql, array $params = []) {
			$stmt = $this->pdo->execute();
			if (!empty($params)) {
				foreach($params as $key => $value) {
					$statement = $sql->bindValue(':'.$key, $value)
				}
			}
			return stmt->execute();
		}

		public function row(string $sql, array $params = []):array {
			return $this->query($params)->fetchAll(PDO::FETCH_ASSOC);
		}

		public function column(string $sql, array $params = []):array {
			return $this->query($params)->fetchColumn();
		}
	}