<?php
	namespace application\core;	

	use application\core\RouterInterface;

	class Router implements RouterInterface
	{
		private $routes = [];
		private $paramas = [];

		public function __construct() {
			$routes = require 'application/config/routes.php';
			foreach($routes as $route => $params) {
				$this->add($route, $params);
			}
		}

		public function run() {
			if ($this->match()) {
				$path = 'application\controllers\\'.ucfirst($this->params['controller']).'Controller';
				if (class_exists($path)) {
					$action = $this->params['action'].'Action';
					if (method_exists($path, $action)) {
						$controller = new $path($this->params);
						$controller->$action();
					} else {
						View::errorCode();
					}
				} else {
					View::errorCode();
				}
			} else {
				View::errorCode();
			}
		}

		private function add(string $route, array $params) {
			$route = '/^'.str_replace('/', '\/', $route).'(\?.*)?$/';
			$this->routes[$route] = $params;
		}

		private function match():bool {			
			$url = trim($_SERVER['REQUEST_URI'], '/');
			foreach($this->routes as $route => $params) {
				if (preg_match($route, $url, $matches)) {
					$this->params = $params;
					return true;
				}
			}
			return false;
		}
	}