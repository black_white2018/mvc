<?php
	namespace application\core;

	use application\core\ModelInterface;
	use application\lib\Db;

	class Model implements ModelInterface
	{
		protected $db;

		public function __construct() {
			$this->db = new Db;
		}
	}