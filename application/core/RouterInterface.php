<?php
	namespace application\core;

	interface RouterInterface
	{
		public function __construct();
		public function run();
	}