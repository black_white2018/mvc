<?php
	namespace application\core;

	use application\core\View;

	abstract class Controller
	{
		public $view;
		public $model;
		private $params = [];

		public function __construct(array $params) {
			$this->view = new View($params);
			$this->params = $params;
			$this->model = $this->loadModel();
		}

		private function loadModel() {
			$path = 'application\models\\'.ucfirst($this->params['controller']).'Model';
			if (class_exists($path)) {
				return new $path;
			} else {
				View::errorCode();
			}
		}
	}