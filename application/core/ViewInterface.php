<?php
	namespace application\core;

	interface ViewInterface
	{
		public function __construct(array $params);
		public function render(array $vars);
		public static function errorCode(int $code);
	}