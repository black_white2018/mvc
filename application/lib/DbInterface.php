<?php
	namespace application\lib;

	interface DbInterface
	{
		public function __construct();
		public function row(string $sql, array $params = []):array;
		public function column(string $sql, array $params = []):array;
	}