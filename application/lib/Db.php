<?php
	namespace application\lib;

	use application\lib\DbInterface;
	use PDO;

	class Db implements DbInterface
	{
		private $db;

		public function __construct() {
			try {
				$config = require 'application/config/db.php';
				$this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'].'', $config['dbuser'], $config['dbpassword']);
			} catch (PDOException $error) {
				die($error->getMessage());
			}
		}		

		public function row(string $sql, array $params = []):array {
			return $this->query($sql, $params)->fetchAll(PDO::FETCH_ASSOC);
		}

		public function column(string $sql, array $params = []):array {
			return $this->query($sql, $params)->fetchColumn();
		}

		private function query(string $sql, array $params = []) {
			$statement = $this->db->prepare($sql);
			if (!empty($params)) {
				foreach($params as $key => $value) {
					$this->db->bindValue(':'.$key, $value);
				}
			}
			$statement->execute();
			return $statement;
		}
	}