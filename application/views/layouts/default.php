<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php
			require 'application/views/elems/head.php';
		?>
	</head>
	<body>
		<div id="wrapper">
			<div class="header">
				<?php
					require 'application/views/elems/header.php';
				?>
			</div>
			<div class="main">
				<?php
					echo $content
				?>
			</div>
			<div class="footer">
				<?php
					require 'application/views/elems/footer.php';
				?>				
			</div>
		</div>
	</body>
</html>