<div class="container-fluid sticky-top">
	<div class="container">
		<div class="navbar p-0">
			<a href="/" class="navbar-brand p-0">
				<img src="assets/img/logo2.png" width="200" height="160" class="d-inline-block align-top" alt="логотип">
			</a>
			<nav class="nav" role="navigation">
				<?php
				$routes = require 'application/config/routes.php';
				$active = '';
				$index = '';
				foreach($routes as $route => $params) {
					if ($route == $url) {
						$active = "active";
					}
					if ($route == '') {
						$index = '/';
					}
					echo '<li class="nav-item">', '<a href="'.$index.$route.'" class="nav-link '.$active.'">'.$params['ru'].'</a>', '</li>';
				}
				?>
			</nav>
		</div>
	</div>
</div>