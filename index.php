<?php
	declare(strict_types = 1);
	require 'application/lib/Development.php';

	use application\core\Router;

	spl_autoload_register(function($class) {
		$path = str_replace('\\', '/', $class).'.php';
		if (file_exists($path)) {
			require $path;
		}
	});

	$router = new Router;
	$router->run();
